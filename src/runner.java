import java.io.IOException;
import java.util.List;

class Runner {

    private List<Article> articles;
    private List<Chapter> chapters;


    String run(String[] args) throws IOException {
        String res = "";
        try {
            OptionParser optionParser = new OptionParser();
            String filepath = optionParser.parseArguments(args);
            List<Integer> resultarticles = optionParser.getResultArticles();
            int resutChapter = optionParser.getResultChapter();
            MyFileReader myFileReader = new MyFileReader();
            Constitution constitution = myFileReader.readFile(filepath);
            articles = constitution.articles;
            chapters = constitution.chapters;

            res = selectOutput(resultarticles, resutChapter);
        }catch(IOException e){
            System.out.println("podano zla sciezke do pliku");
        }
        catch (IllegalArgumentException e){
            System.out.println("wprowadzono niepoprawne dane");
        }

        return res;
    }


    private String selectOutput(List<Integer> resultarticles, int resultChapter) {
        StringBuilder output = new StringBuilder();

        for (Integer i : resultarticles) {
            output.append(printArticle(articles.get(i)));
        }
        if (resultChapter != -1) {
            output.append(printChapter(chapters.get(resultChapter)));
        }
        return output.toString();
    }


    private String printArticle(Article article) {
        StringBuilder output = new StringBuilder();
        output.append("Art. ").append(article.getArticleId()).append(".").append("\n");

        String tmp = article.toString();
        for (int i = 0; i < tmp.length() - 1; i++) {

            if (i > 0 && tmp.charAt(i) <= '9' && tmp.charAt(i) >= '0' && tmp.charAt(i + 1) == ')') {
                output.append("\n");
            }

            output.append(tmp.charAt(i));
            if (tmp.charAt(i) == '.' && tmp.charAt(i + 1) <= '9' && tmp.charAt(i + 1) >= '0' && (tmp.charAt(i+2)=='.' || tmp.charAt(i+3)=='.')) {
                output.append("\n");
            }
        }
        output.append(tmp.charAt(tmp.length() - 1));
        output.append("\n");

        return output.toString();

    }


    private String printChapter(Chapter chapter) {
        StringBuilder output = new StringBuilder();
        output.append(chapter.chapterId).append("\n");
        output.append(chapter.chapterTitle).append("\n");
        for (int i = chapter.fromArticleId; i <= chapter.toArticleId; i++) {
            output.append(printArticle(articles.get(i)));
        }
        return output.toString();
    }


}