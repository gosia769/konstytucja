import java.lang.String;

/**
 * Created by Gosia on 28/11/2016.
 */
public class Article {

    private StringBuilder articleContent = new StringBuilder();
    private int articleId;


    Article(String articleContent, int articleId) {
        this.articleContent.append(articleContent);
        this.articleId = articleId;
    }

    @Override
    public String toString() {
        return articleContent.toString();
    }

    public int getArticleId() {
        return articleId;
    }

    public void addLine(String newLine){
        articleContent.append(newLine);
    }
}
