import java.io.IOException;

/**
 * Created by Gosia on 06/12/2016.
 */
public class Tester {
    public static void main(String[] args) throws IOException {
        Runner runner = new Runner();
        System.out.print(runner.run(args));

    }
}
