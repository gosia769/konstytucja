import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Gosia on 28/11/2016.
 */
class MyFileReader {

    Constitution readFile(String filepath) throws IOException {

        LineAssigner lineAssigner = new LineAssigner();

        java.io.FileReader fileReader = new java.io.FileReader(filepath);   // wskaznik na plik
        Scanner in = new Scanner(fileReader);

        try {
            while (in.hasNextLine()) {
                lineAssigner.assignLine(in.nextLine());
            }
        } finally {
            try {
                fileReader.close();
            } catch (IOException ex) {
                System.out.println("nie udalo sie zamknac pliku");
            }
        }
        return lineAssigner.getConstitution();
    }
}
