/**
 * Created by Gosia on 28/11/2016.
 */
public class Chapter {

    String chapterId;
    String chapterTitle;
    int fromArticleId;
    int toArticleId;

    Chapter(String chapterId,String chapterTitle, int fromArticleId, int toArticleId){
        this.chapterId  = chapterId;
        this.chapterTitle = chapterTitle;
        this.fromArticleId  = fromArticleId;
        this.toArticleId = toArticleId;
    }
}
