import static java.lang.Character.isDigit;

/**
 * Created by Gosia on 04/12/2016.
 */


public class LineAssigner {

    private Constitution constitution = new Constitution();
    private int chapterIndex = 0;
    private int articleIndex = 0;
    private boolean chapterStarts = true;


    void assignLine(String newline) {


        if (newline.startsWith("Rozdzia")) {
            chapterIndex++;
            constitution.chapters.add(new Chapter(newline, "", articleIndex, 0));
            constitution.chapters.get(chapterIndex - 1).toArticleId = articleIndex;
            constitution.chapters.get(chapterIndex).fromArticleId = articleIndex + 1;
            chapterStarts = true;
        } else if (newline.startsWith("Art. ")) {
            articleIndex++;
            constitution.articles.add(new Article("", articleIndex));
            chapterStarts = false;
        } else if (chapterStarts && newline.toUpperCase() == newline && !isDigit(newline.charAt(0))){
            if (newline.endsWith("-")) {
                constitution.chapters.get(chapterIndex).chapterTitle += newline.substring(0, newline.length() - 1);
            } else {
                constitution.chapters.get(chapterIndex).chapterTitle += newline;
                constitution.chapters.get(chapterIndex).chapterTitle += " ";
            }
        } else if (!newline.contains("©Kancelaria Sejmu") && !newline.contains("2009-11-16")) {
            if(newline.toUpperCase() == newline && !isDigit(newline.charAt(0))){
                constitution.articles.get(articleIndex).addLine("\n");
            }
            if (newline.endsWith("-")) {
                constitution.articles.get(articleIndex).addLine(newline.substring(0, newline.length() - 1));
            } else if (newline.endsWith(".")) {
                constitution.articles.get(articleIndex).addLine(newline);
            } else {
                constitution.articles.get(articleIndex).addLine(newline);
                constitution.articles.get(articleIndex).addLine(" ");

            }
        }
    }

    Constitution getConstitution() {
        return this.constitution;
    }

}
