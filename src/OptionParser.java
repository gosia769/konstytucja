import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gosia on 28/11/2016.
 */
public class OptionParser {

    private int resultChapter = -1;
    private List<Integer> resultarticles = new ArrayList<>();

    String parseArguments(String[] arguments)throws IllegalArgumentException {
        String filepath="";

            if (arguments.length < 3) {
                throw new IllegalArgumentException();
            }

            filepath = arguments[0];
            int i = 1;

            if (!arguments[1].equals("r") && !arguments[1].equals("a")) {
                throw new IllegalArgumentException();
            }


            if (arguments[i].equals("a")) {
                i++;
                while (i < arguments.length && !arguments[i].equals("r")) {

                    if (arguments[i].contains("-")) {     // jesli jest przedzial
                        String[] parts = arguments[i].split("-");
                        String part1 = parts[0];
                        String part2 = parts[1];

                        if (Integer.valueOf(part1) <= Integer.valueOf(part2)) {
                            for (int j = Integer.parseInt(part1); j <= Integer.parseInt(part2); j++) {
                                if (j < 244 && j > 0) {
                                    resultarticles.add(j);
                                } else {
                                    throw new IllegalArgumentException();
                                }
                            }
                        } else {
                            for (int j = Integer.parseInt(part2); j <= Integer.parseInt(part1); j++) {
                                if (j < 244 && j > 0) {
                                    resultarticles.add(j);
                                } else {
                                    throw new IllegalArgumentException();
                                }
                            }
                        }
                    }
                    //jesli sa wybrane artykuly
                    else if (Integer.valueOf(arguments[i]) < 244 && Integer.valueOf(arguments[i]) > 0) {
                        resultarticles.add(Integer.valueOf(arguments[i]));
                    } else {
                        throw new IllegalArgumentException();
                    }
                    i++;
                }

            }
            //sprawdzam czy jest podany jeszcze rozdzial
            if (i < arguments.length && arguments[i].equals("r")) {
                if (Integer.valueOf(arguments[i + 1]) > 0 &&  (Integer.valueOf(arguments[i + 1]) < 14)) {
                    resultChapter = Integer.valueOf(arguments[i + 1]);
                } else {
                    throw new IllegalArgumentException();
                }

        }
        return filepath;

    }



    public List<Integer> getResultArticles() {
        return this.resultarticles;
    }

    public int getResultChapter() {
        return this.resultChapter;
    }
}
//ArrayIndexOutOfBoundsException
//illegalargumentexception