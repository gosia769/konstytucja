import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Gosia on 06/12/2016.
 */
public class RunnerTest{

 @Test
    public void testRun() throws Exception{
     assertEquals("Art. 202.\n" +
                     "1. Najwyższa Izba Kontroli jest naczelnym organem kontroli państwowej.\n" +
                     "2. Najwyższa Izba Kontroli podlega Sejmowi.\n" +
                     "3. Najwyższa Izba Kontroli działa na zasadach kolegialności.\n",

             new Runner().run(new String[] {"C:\\Users\\Gosia\\Documents\\lab8\\konstytucja.txt", "a", "202"}));

     assertEquals("Art. 1." + "\n" +
                     "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.\n",
             new Runner().run(new String[] {"C:\\Users\\Gosia\\Documents\\lab8\\konstytucja.txt", "a", "1"}));
     assertEquals("Rozdział X\n" +
             "FINANSE PUBLICZNE \n" +
             "Art. 216.\n" +
             "1. Środki finansowe na cele publiczne są gromadzone i wydatkowane w sposób określony w ustawie.\n" +
             "2. Nabywanie, zbywanie i obciążanie nieruchomości, udziałów lub akcji oraz emisja papierów wartościowych przez Skarb Państwa, Narodowy Bank Polski lub inne państwowe osoby prawne następuje na zasadach i w trybie określonych w ustawie.\n" +
             "3. Ustanowienie monopolu następuje w drodze ustawy.\n" +
             "4. Zaciąganie pożyczek oraz udzielanie gwarancji i poręczeń finansowych przez państwo następuje na zasadach i w trybie określonych w ustawie.\n" +
             "5. Nie wolno zaciągać pożyczek lub udzielać gwarancji i poręczeń finansowych, w następstwie których państwowy dług publiczny przekroczy 3/5 wartości rocznego produktu krajowego brutto. Sposób obliczania wartości rocznego produktu krajowego brutto oraz państwowego długu publicznego określa ustawa.\n" +
             "Art. 217.\n" +
             "Nakładanie podatków, innych danin publicznych, określanie podmiotów, przedmiotów opodatkowania i stawek podatkowych, a także zasad przyznawania ulg i umorzeń oraz kategorii podmiotów zwolnionych od podatków następuje w drodze ustawy.\n" +
             "Art. 218.\n" +
             "Organizację Skarbu Państwa oraz sposób zarządzania majątkiem Skarbu Państwa określa ustawa.\n" +
             "Art. 219.\n" +
             "1. Sejm uchwala budżet państwa na rok budżetowy w formie ustawy budżetowej.\n" +
             "2. Zasady i tryb opracowania projektu budżetu państwa, stopień jego szczegółowości oraz wymagania, którym powinien odpowiadać projekt ustawy budżetowej, a także zasady i tryb wykonywania ustawy budżetowej określa ustawa.\n" +
             "3. W wyjątkowych przypadkach dochody i wydatki państwa w okresie krótszym niż rok może określać ustawa o prowizorium budżetowym. Przepisy dotyczące projektu ustawy budżetowej stosuje się odpowiednio do projektu ustawy o prowizorium budżetowym.\n" +
             "4. Jeżeli ustawa budżetowa albo ustawa o prowizorium budżetowym nie weszły w życie w dniu rozpoczęcia roku budżetowego, Rada Ministrów prowadzi gospodarkę finansową na podstawie przedłożonego projektu ustawy.\n" +
             "Art. 220.\n" +
             "1. Zwiększenie wydatków lub ograniczenie dochodów planowanych przez Radę Ministrów nie może powodować ustalenia przez Sejm większego deficytu budżetowego niż przewidziany w projekcie ustawy budżetowej.\n" +
             "2. Ustawa budżetowa nie może przewidywać pokrywania deficytu budżetowego przez zaciąganie zobowiązania w centralnym banku państwa.\n" +
             "Art. 221.\n" +
             "Inicjatywa ustawodawcza w zakresie ustawy budżetowej, ustawy o prowizorium budżetowym, zmiany ustawy budżetowej, ustawy o zaciąganiu długu publicznego oraz ustawy o udzielaniu gwarancji finansowych przez państwo przysługuje wyłącznie Radzie Ministrów.\n" +
             "Art. 222.\n" +
             "Rada Ministrów przedkłada Sejmowi najpóźniej na 3 miesiące przed rozpoczęciem roku budżetowego projekt ustawy budżetowej na rok następny. W wyjątkowych przypadkach możliwe jest późniejsze przedłożenie projektu.\n" +
             "Art. 223.\n" +
             "Senat może uchwalić poprawki do ustawy budżetowej w ciągu 20 dni od dnia przekazania jej Senatowi.\n" +
             "Art. 224.\n" +
             "1. Prezydent Rzeczypospolitej podpisuje w ciągu 7 dni ustawę budżetową albo ustawę o prowizorium budżetowym przedstawioną przez Marszałka Sejmu. Do ustawy budżetowej i ustawy o prowizorium budżetowym nie stosuje się przepisu art. 122 ust. 5.\n" +
             "2. W przypadku zwrócenia się Prezydenta Rzeczypospolitej do Trybunału Konstytucyjnego w sprawie zgodności z Konstytucją ustawy budżetowej albo ustawy o prowizorium budżetowym przed jej podpisaniem, Trybunał orzeka w tej sprawie nie później niż w ciągu 2 miesięcy od dnia złożenia wniosku w Trybunale.\n" +
             "Art. 225.\n" +
             "Jeżeli w ciągu 4 miesięcy od dnia przedłożenia Sejmowi projektu ustawy budżetowej nie zostanie ona przedstawiona Prezydentowi Rzeczypospolitej do podpisu, Prezydent Rzeczypospolitej może w ciągu 14 dni zarządzić skrócenie kadencji Sejmu.\n" +
             "Art. 226.\n" +
             "1. Rada Ministrów w ciągu 5 miesięcy od zakończenia roku budżetowego przedkłada Sejmowi sprawozdanie z wykonania ustawy budżetowej wraz z informacją o stanie zadłużenia państwa.\n" +
             "2. Sejm rozpatruje przedłożone sprawozdanie i po zapoznaniu się z opinią Najwyższej Izby Kontroli podejmuje, w ciągu 90 dni od dnia przedłożenia Sejmowi sprawozdania, uchwałę o udzieleniu lub o odmowie udzielenia Radzie Ministrów absolutorium.\n" +
             "Art. 227.\n" +
             "1. Centralnym bankiem państwa jest Narodowy Bank Polski. Przysługuje mu wyłączne prawo emisji pieniądza oraz ustalania i realizowania polityki pieniężnej.Narodowy Bank Polski odpowiada za wartość polskiego pieniądza.\n" +
             "2. Organami Narodowego Banku Polskiego są: Prezes Narodowego Banku Polskiego, Rada Polityki Pieniężnej oraz Zarząd Narodowego Banku Polskiego.\n" +
             "3. Prezes Narodowego Banku Polskiego jest powoływany przez Sejm na wniosek Prezydenta Rzeczypospolitej na 6 lat.\n" +
             "4. Prezes Narodowego Banku Polskiego nie może należeć do partii politycznej, związku zawodowego ani prowadzić działalności publicznej nie dającej się pogodzić z godnością jego urzędu.\n" +
             "5. W skład Rady Polityki Pieniężnej wchodzą Prezes Narodowego Banku Polskiego jako przewodniczący oraz osoby wyróżniające się wiedzą z zakresu finansów, powoływane na 6 lat, w równej liczbie przez Prezydenta Rzeczypospolitej, Sejm i Senat.\n" +
             "6. Rada Polityki Pieniężnej ustala corocznie założenia polityki pieniężnej i przedkłada je do wiadomości Sejmowi równocześnie z przedłożeniem przez Radę Ministrów projektu ustawy budżetowej. Rada Polityki Pieniężnej, w ciągu 5 miesięcy od zakończenia roku budżetowego, składa Sejmowi sprawozdanie z wykonania założeń polityki pieniężnej.\n"+
             "7. Organizację i zasady działania Narodowego Banku Polskiego oraz szczegółowe zasady powoływania i odwoływania jego organów określa ustawa.\n",

             new Runner().run(new String[] {"C:\\Users\\Gosia\\Documents\\lab8\\konstytucja.txt", "r", "10"}));
 }


}